FROM registry.gitlab.com/arise-biodiversity/dsi/algorithm-containers/images/megadetector:v0.1 as models

# set base image (host OS)
FROM pytorch/pytorch:2.2.1-cuda12.1-cudnn8-runtime

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    ffmpeg \
    libsm6 \
    libxext6 \
    git \
    wget \
    && rm -rf /var/lib/apt/lists/*

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

# copy code
COPY src/ .

# download the megadetector models
# RUN wget -O /code/md_v4.1.0.pb https://lilablobssc.blob.core.windows.net/models/camera_traps/megadetector/md_v4.1.0/md_v4.1.0.pb
# RUN wget -O /code/md_v5a.0.0.pt https://github.com/agentmorris/MegaDetector/releases/download/v5.0/md_v5a.0.0.pt
# RUN wget -O /code/md_v5b.0.0.pt https://github.com/agentmorris/MegaDetector/releases/download/v5.0/md_v5b.0.0.pt
COPY --from=models /code/md_v4.1.0.pb /code/md_v4.1.0.pb
COPY --from=models /code/md_v5a.0.0.pt /code/md_v5a.0.0.pt
COPY --from=models /code/md_v5b.0.0.pt /code/md_v5b.0.0.pt

# clone megadetector repos
#RUN git clone https://github.com/Microsoft/cameratraps
#WORKDIR /code/cameratraps
#RUN git reset --hard fc8cdf71064b54241a63db9c80bf35e117886911

RUN git clone https://github.com/agentmorris/megadetector && cd megadetector && git reset --hard 9a0e1c3ad3ab63301489313f6f6a8768cfb5018e

#WORKDIR /code
RUN git clone https://github.com/Microsoft/ai4eutils && cd ai4eutils && git reset --hard ff141cd6b7a7b504c18b0987fbfd58ec6552df43
#WORKDIR /code/ai4eutils
#RUN git reset --hard 1bbbb8030d5be3d6488ac898f9842d715cdca088

#WORKDIR /code
RUN git clone https://github.com/ecologize/yolov5/ && cd yolov5 && git reset --hard ad033704d1a826e70cd365749e1bb01f1ea8282a
#RUN git clone https://github.com/ultralytics/yolov5/
#WORKDIR /code/yolov5
#RUN git reset --hard c23a441c9df7ca9b1f275e8c8719c949269160d1

#WORKDIR /code
#ENV PYTHONPATH "${PYTHONPATH}:/code/cameratraps:/code/ai4eutils:/code/yolov5"

ENV PYTHONPATH "${PYTHONPATH}:/code/megadetector:/code/ai4eutils:/code/yolov5"
ENV LD_LIBRARY_PATH "${LD_LIBRARY_PATH}:/opt/conda/lib"

# Install cudnn for MD 4.1
#RUN conda install -c anaconda cudnn
RUN wget https://bootstrap.pypa.io/pip/3.6/get-pip.py && \
	python3 get-pip.py --user && \
	rm get-pip.py
# RUN pip install torchvision==0.15.2
# RUN pip install torch==2.0.1
RUN pip install --upgrade fastapi pydantic python-multipart uvicorn jinja2 Pillow gpustat
# copy the content of the local src directory to the working directory
COPY src/ .

# hack to fix problems with torch >= 1.11.0
RUN sed -i 's/return F.interpolate(input, self.size, self.scale_factor, self.mode, self.align_corners,/return F.interpolate(input, self.size, self.scale_factor, self.mode, self.align_corners)/g' /opt/conda/lib/python3.10/site-packages/torch/nn/modules/upsampling.py
RUN sed -i 's/recompute_scale_factor=self.recompute_scale_factor)//g' /opt/conda/lib/python3.10/site-packages/torch/nn/modules/upsampling.py

COPY src/website_files/ .
# run megadetector wrapper script
CMD [ "python", "./megadetector.py" ]
# CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "9976", "--reload"]