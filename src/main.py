import json
import shutil
import tempfile
from typing import List
import os
from PIL import Image
import gpustat
from pathlib import Path
from datetime import datetime

from fastapi import FastAPI, File, UploadFile, HTTPException, Request
from fastapi.responses import HTMLResponse, FileResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from pprint import pprint

from pathlib import Path
import subprocess

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")


templates = Jinja2Templates(directory="templates")

def convert_to_arise(json: json):
    json_imges = json["images"]
    for image in json_imges:
        for detections in image["detections"]:
            detected_category = json["detection_categories"][detections["category"]]
            pred_boxes_list = detections["bbox"]
            scores_list = round(detections["conf"], 2)
        path = image["file"]
        file_name = Path(image["file"]).name
        pred_boxes = pred_boxes_list
        score = scores_list
    row_json = []
    row_json.append([path, file_name, pred_boxes, score, detected_category])
    
    res = {
            "generated_by": {
                "datetime": datetime.now().isoformat(),
                "version": "0.6",
                "tag": "DT2HD"
            },
            "media": [],
            "region_groups": [],
            "predictions": [],
    }
    
    media_list = []
    region_groups_list = []
    predictions_list = []
    #  row_json.append([path, file_name, pred_boxes, score])
    for row in row_json:
        print(f"row: {row}")
        media_list.append({"id": row[0], "filename": row[1]})
        region_groups_list.append({"id": "",
                                # After reading the docs im quite sure we can delete the individual_id 
                                # "individual_id": "",
                                "regions": [
                                        {
                                            "media_id": row[0], 
                                            "box": {
                                                "x1": row[2][0],
                                                "y1": row[2][1],
                                                "x2": row[2][2],
                                                "y2": row[2][3],
                                            }
                                        }
                                    ]
                                })
        predictions_list.append(
            {
                "region_group_id": row[0],
                "taxa": {
                    "type": row[4],
                    "items": [{
                        "scientific_name": row[4],
                        "probability": row[3],
                        "scientific_name_id": "https://data.biodiversitydata.nl/naturalis/specimen/RGM.1332465"
                    }]
                }
            
            }
        )
    res["media"] = media_list
    res["region_groups"] = region_groups_list
    res["predictions"] = predictions_list
    return res

# @app.get("/items/{id}", response_class=HTMLResponse)
# async def read_item(request: Request, id: str):
#     return templates.TemplateResponse("item.html", {"request": request, "id": id})

@app.get("/gpus")
async def list_gpus():
    return gpustat.new_query().jsonify()

@app.get("/cam", response_class=HTMLResponse)
def get_camera(request: Request): 
    return templates.TemplateResponse("cam.html", {"request": request})
  
@app.get("/tmp/{file_path:path}")
def get_image(
    file_path: str
):
    return FileResponse(f"./outputs/tmp/{file_path}")

@app.post("/live-cam")
async def stream_camera(
    
):
    try:
        p = subprocess.run(["python3", "demo/demo.py", "--webcam"])
            # p = subprocess.run(f"python3 demo/demo.py --input {inputs} --output {out_tmp}", shell=True)
    except FileNotFoundError as fnfe:
            raise HTTPException(status_code=500, detail=f"While running the algorithm, an error occured: {fnfe.strerror}")

@app.post("/v1/analyse/")
async def create_upload_files(
    media: List[UploadFile] = File(description="Multiple files as UploadFile"),
):
    with tempfile.TemporaryDirectory() as tmpdirname:
        out_tmp = str(Path(f"./outputs{tmpdirname}/"))
        
        inputs = []
        for file in media:
            file_location = str(Path(f"{tmpdirname}/{file.filename}"))
            with open(file_location, "wb+") as file_object:
                shutil.copyfileobj(file.file, file_object) 
            inputs.append(file_location)

        # Debugging
        print("File:", file_location) # print the name of the file
        print("out_tmp:", out_tmp) # print the name of the file
        print("Exists:", os.path.exists(file_location)) # check if the file exists
        print("Is file:", os.path.isfile(file_location)) # check if it's a file (not a directory)

        # try:
        #     img = Image.open(file_location) # try opening the image
        #     img.verify() # verify that it is, in fact an image
        # except (IOError, SyntaxError) as e:
        #     print('Bad file:', file_location) # print out the names of bad files
        
        Path(out_tmp).mkdir(parents=True, exist_ok=True)
        
        try:
            p = subprocess.run(["python", "./megadetector.py", "--input"] + inputs + ["--output", out_tmp])
            # p = subprocess.run(f"python3 demo/demo.py --input {inputs} --output {out_tmp}", shell=True)
        except FileNotFoundError as fnfe:
            raise HTTPException(status_code=500, detail=f"While running the algorithm, an error occured: {fnfe.strerror}")

        # if p.returncode != 0:
        #     print(p.stderr)
        #     raise HTTPException(status_code=500, detail="While running the algorithm, an error occured")
    
        data_json_location = Path(f"{out_tmp}/data.json")
        print(f"trying to get json from: {data_json_location}")
        if data_json_location.exists():
            with open(data_json_location) as f:
                returned_json = json.load(f)
            pprint(returned_json)
            return convert_to_arise(returned_json)
        else:
            raise HTTPException(status_code=500, detail=f"did not find {data_json_location}")
            


@app.get("/", response_class=HTMLResponse)
async def main(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})
