import os

# ARISE DSI team: print the CUDA/GPU device count and the GPU device names.
import torch  # type: ignore

from lib_common import read_yaml  # type: ignore
from lib_command import create_command  # type: ignore


# ARISE DSI team: print the CUDA/GPU device count and the GPU device names.
print(f"CUDA device count: {torch.cuda.device_count()}.")
for device_index in range(torch.cuda.device_count()):
    device_name = torch.cuda.get_device_name(device_index)
    print(f"GPU device name {device_index + 1}: {device_name}.")
# ARISE DSI team: print the CUDA/GPU device count and the GPU device names.


os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

config = read_yaml("config.yaml")
for conf_key in config.keys():
    if conf_key in os.environ:
        config[conf_key] = os.environ[conf_key]

md_cmd = create_command(config)
print(md_cmd)
os.system(md_cmd)
